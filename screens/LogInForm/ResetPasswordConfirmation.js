import React from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';

export default function ResetPasswordConfirmation() {

  function changePassword() {
    setPage("recovered");
  }

  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={require('./logo.png')} />
      <View style={styles.formContainer}>
        <Text style={styles.heading}>Reset Password</Text>
        
        <Text style={styles.subtitle}>New Password</Text>
        <TextInput
          style={styles.input}
          placeholder="********"
          placeholderTextColor="rgba(0, 0, 0, 0.5)" 
          secureTextEntry
        />
        
        <Text style={styles.subtitle}>Confirm password</Text>
        <TextInput
          style={styles.input}
          placeholder="********"
          placeholderTextColor="rgba(0, 0, 0, 0.5)" 
          secureTextEntry
        />
        
        <TouchableOpacity style={styles.codeButton} onPress={() => changePassword()}>
          <Text style={styles.codeButtonText}>Reset Password</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  formContainer: {
    width: '80%',
    marginBottom: 50, // Add some margin to separate form elements
  },
  input: {
    width: '100%', // Take up the full width of the form container
    height: 50,
    borderWidth: 1,
    borderColor: 'turquoise',
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    textAlign: 'center',
  },
  codeButton: {
    backgroundColor: 'turquoise',
    padding: 10,
    borderRadius: 10,
    width: '50%', // Take up 50% of the form container width
    height: 40,
    alignSelf: 'center', // Center the button horizontally
    marginTop: 20, // Add margin to the top of the button
  },
  codeButtonText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
  heading: {
    fontSize: 30,
    textAlign: 'center',
    marginBottom: 170, // Adjusted margin
    color: 'turquoise',
  },
  subtitle: {
    fontSize: 15,
    fontWeight: 'bold',
    
    marginBottom: 10, // Adjusted margin
    color: 'turquoise',
  },
  logo: {
    width: 130,
    height: 130,
    resizeMode: 'contain',
    marginBottom: 13,
    marginRight: 17,
  },
});
