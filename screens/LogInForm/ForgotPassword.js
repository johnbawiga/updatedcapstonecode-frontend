import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';

export default function ForgotPassword() {
  const [email, setEmail] = useState('');
  const navigation = useNavigation();

  const handleForgotPassword = async () => {
    try {
      const response = await axios.post('http://localhost:8080/user/forgotpassword', {
        email: email,
      });
      console.log(response.data);
      navigation.navigate('ResetPassword', { email: email });
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={require('./logo.png')} />
      <View>
        <Text style={styles.heading}>Password Reset</Text>
        <Text style={styles.subtitle}>Enter your email address</Text>
        <TextInput 
          style={styles.email}
          placeholder="Johndoe@gmail.com"
          value={email}
          placeholderTextColor="rgba(0, 0, 0, 0.5)" 
          onChangeText={(text) => setEmail(text)}
        />
        <TouchableOpacity style={styles.codeButton} onPress={() => navigation.navigate('ResetPasswordConfirmation')}>
          <Text style={styles.codeButtonText}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    
  },
  email: {
    width: 275,
    height: 50,
    borderWidth: 1,
    borderColor: 'turquoise',
    marginBottom: 100,
    paddingHorizontal: 10,
    borderRadius: 10,
    textAlign: 'center',
  },
  codeButton: {
    backgroundColor: 'turquoise',
    padding: 10,
    borderRadius: 10,
    width: 100,
    height: 40,
    marginBottom: 55,
  },
  codeButtonText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
  heading: {
    fontSize: 30,
    textAlign: 'center',
    marginBottom: 100,
    color: 'turquoise',
  },
  subtitle: {
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 70,
    color: 'turquoise',
  },
  logo: {
    width: 130,
    height: 130,
    resizeMode: 'contain',
    marginBottom: 40,
    marginRight: 17,
  },
});
